# node required for the build
FROM node:14.15.3-alpine3.12 as build
LABEL Author:  semleti

# install tooles required to build the frontend
RUN apk add git python2 make g++

WORKDIR /
RUN git clone https://github.com/KlausSchaefers/quant-ux.git

WORKDIR /quant-ux/

# install missing dependencies
RUN npm install postcss-normalize-repeat-style cssstyle@2.2.0 --save && \
    # install dependencies
    npm i && \
    # build the frontend
    npm run build


FROM nginx:1.19.6 as release
LABEL Author:  semleti

COPY --from=build /quant-ux/dist /app/

# overwrite nginx.conf by mounting a volume to /etc/nginx/nginx.conf