# This repo is outdated/broken

It was designed to work only with version 3.0.4.

The newer versions of the quant-ux include a Dockerfile each, so I suggest using those.

The docker-compose.yml of this repo can be used as inspiration should you want one for the newer versions of quant-ux. You will have to rename the Dockerfile paths, as well as the image names.

# Dockerized [Quant-UX](https://github.com/KlausSchaefers)

Quant-UX is "The OpenSource prototyping and user research tool."

You can find a working demo at [https://quant-ux.com/#/](https://quant-ux.com/#/)

This repo is used to dockerize quant-UX to allow for easier deployment.

## How to use it

1. Install docker and docker-compose. [official instructions](https://docs.docker.com/get-docker/)
2. Clone this repository an cd into it. `git clone https://gitlab.com/semleti/quant-ux-docker`
3. Configure the app by modifying [matc.conf](matc.conf), [nginx.conf](nginx.conf) and [docker-compose.yml](docker-compose.yml) (Optionnal)
4. Run `docker-compose up`.
5. Navigate to [http://localhost:3000](http://localhost:3000) to view the frontend. (replace `localhost` by th IP of the docker host if needed)

## Structure

1. [server.dockerfile](#server.dockerfile)
2. [frontend.dockerfile](#frontend.dockerfile)
3. [docker-compose.yml](#docker-compose.yml)

### server.dockerfile

The (server.dockerfile)[server.dockerfile] is used to build the container for the server of quantum-UX.

It uses the `jamesdbloom/docker-java8-maven` image to build the fat-jar.

The code is pulled from [https://github.com/KlausSchaefers/quant-ux](https://github.com/KlausSchaefers/quant-ux)

The fat jar is then copied into the `openjdk:8-alpine` image.

### frontend.dockerfile

The [frontend.dockerfile](frontend.dockerfile) is used to build the container for the frontend of quantum-UX.

The code is pulled form [https://github.com/KlausSchaefers/quant-ux](https://github.com/KlausSchaefers/quant-ux)

The dist folder is copied into the `nginx:1.19.6` image.

### docker-compose.yml

The [docker-compose.yml](docker-compose.yml) is used to launch a docker-compose.

3 services get created:

1. quant-ux-server on port `8080` and mounts a volume for the image storage, and mounts (matc.conf)[matc.conf] to configure it
2. quant-ux-frontend on port ` 3030` and mouns (nginx.conf) to configure it
3. mongo and mounts volume to persist the database data
