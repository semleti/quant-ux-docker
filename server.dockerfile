# Java 8 with maven required for the build
FROM jamesdbloom/docker-java8-maven as build
LABEL Author:  semleti

WORKDIR /
RUN git clone https://github.com/KlausSchaefers/qux-java.git

WORKDIR /qux-java/

# build the fat-jar
RUN mvn clean package


FROM openjdk:8-alpine as release
LABEL Author:  semleti

COPY --from=build /qux-java/target/server-3.0.4-fat.jar /app/

WORKDIR /app/

# overwrite matc.conf by mounting a volume to /app/matc.conf

# start the server
ENTRYPOINT  ["java", "-jar", "server-3.0.4-fat.jar", "-conf", "matc.conf", "-instances", "1"]